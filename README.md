# Examen Mercado Libre

Magneto quiere reclutar la mayor cantidad de mutantes para poder luchar
contra los X-Men.

Te ha contratado a ti para que desarrolles un proyecto que detecte si un
humano es mutante basándose en su secuencia de ADN.

Para eso te ha pedido crear un programa con un método o función con la siguiente firma (En
alguno de los siguiente lenguajes: Java / Golang / C-C++ / JavaScript (Node) / Python / Ruby):

`boolean isMutant(String[] dna);`

En donde recibirás como parámetro un array de Strings que representan cada fila de una tabla
de (NxN) con la secuencia del ADN. Las letras de los Strings solo pueden ser: (A, T, C, G), las
cuales representa cada base nitrogenada del ADN.

Sabrás si un humano es mutante, si encuentras **más de una secuencia de cuatro letras
iguales** , de forma oblicua, horizontal o vertical.

## API */mutants*
### Cómo funciona
1. A través de Postman o un cliente web ejecutar la siguiente solicitud:

- **Método:** `POST`
- **Endpoint:** `http://95.111.251.156:8080/mutants`
- **Request Body**: 
  - `{"dna": ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]} // Mutante`
  - `{"dna": ["ATGCGA","CAGTTC","TTATGT","AGAAGG","TCTCTA","TCACTG"]"} // Humano`
- **Content Type:** `application/json`

2. Un ADN mutante es aquel que tenga más de una secuencia de cuatro letras iguales en la matriz ACGT enviada; ya sea de manera horizantal, vertical o diagonal.
3. Si se envía un ADN mutante, la respuesta será `HTTP 200-OK`, de lo contrario será `HTTP 403-FORBIDDEN`.

## API */stats*
### Cómo funciona
1. A través de Postman o un cliente web ejecutar la siguiente solicitud:

- **Método:** `GET`
- **Endpoint:** `http://95.111.251.156:8080/stats`

2. La respuesta será el resumen de los ADN's analizados anteriormente en el endpoint `/mutants`. Se indicará la cantidad de ADN mutante, ADN humano y el radio de relación entre los registros guardados.

## Documentación

La documentación OpenAPI del proyecto se encuentra en:

`http://95.111.251.156:8080/swagger-ui/index.html`