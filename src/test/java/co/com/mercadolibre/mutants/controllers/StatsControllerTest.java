package co.com.mercadolibre.mutants.controllers;

import co.com.mercadolibre.mutants.dto.StatsDTO;
import co.com.mercadolibre.mutants.services.DNARecordService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class StatsControllerTest {

    @Mock
    private DNARecordService mockService;

    private StatsController controller;

    @BeforeEach
    void setUp() {
        controller = new StatsController(mockService);
    }

    @Test
    void testStats() {
        StatsDTO expected = new StatsDTO(2, 1, 2.0);
        Mockito.when(mockService.processStats()).thenReturn(expected);

        ResponseEntity<StatsDTO> actualResponse = controller.stats();
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.OK, actualResponse.getStatusCode());

        StatsDTO actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals(expected.getCountMutantDna(), actualResponseBody.getCountMutantDna());
        Assertions.assertEquals(expected.getCountHumanDna(), actualResponseBody.getCountHumanDna());
        Assertions.assertEquals(expected.getRatio(), actualResponseBody.getRatio());
    }
}