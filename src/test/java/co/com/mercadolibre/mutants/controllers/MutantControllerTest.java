package co.com.mercadolibre.mutants.controllers;

import co.com.mercadolibre.mutants.dto.MutantRequest;
import co.com.mercadolibre.mutants.dto.MutantResponse;
import co.com.mercadolibre.mutants.services.DNARecordService;
import co.com.mercadolibre.mutants.services.MutantService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class MutantControllerTest {

    @Mock
    private MutantService mockMutantService;

    @Mock
    private DNARecordService mockDNARecordService;

    private MutantController controller;

    @BeforeEach
    void setUp() {
        controller = new MutantController(mockMutantService, mockDNARecordService);
    }

    @Test
    void test200Response() {
        String[] mutantDNA = {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};
        MutantRequest request = new MutantRequest(mutantDNA);

        Mockito.doNothing().when(mockDNARecordService).processSave(request.getDna(), true);
        Mockito.when(mockMutantService.isMutant(request.getDna())).thenReturn(true);

        ResponseEntity<MutantResponse> actualResponse = controller.mutant(request);
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.OK, actualResponse.getStatusCode());

        MutantResponse actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals("Magneto te da la bienvenida a la Hermandad de Mutantes", actualResponseBody.getMessage());
    }

    @Test
    void test403Response() {
        String[] humanDNA = {"ATGCGA", "CAGTTC", "TTATGT", "AGAAGG", "TCTCTA", "TCACTG"};
        MutantRequest request = new MutantRequest(humanDNA);

        Mockito.doNothing().when(mockDNARecordService).processSave(request.getDna(), false);
        Mockito.when(mockMutantService.isMutant(request.getDna())).thenReturn(false);

        ResponseEntity<MutantResponse> actualResponse = controller.mutant(request);
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.FORBIDDEN, actualResponse.getStatusCode());

        MutantResponse actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals("Lo siento, no puedes unirte a la causa mutante", actualResponseBody.getMessage());
    }

    @Test
    void test400ResponseNullRequestBody() {
        ResponseEntity<MutantResponse> actualResponse = controller.mutant(null);
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, actualResponse.getStatusCode());

        MutantResponse actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals("No indicaste el ADN a analizar", actualResponseBody.getMessage());
    }

    @Test
    void test400ResponseEmptyDNA() {
        MutantRequest request = new MutantRequest(null);

        ResponseEntity<MutantResponse> actualResponse = controller.mutant(request);
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, actualResponse.getStatusCode());

        MutantResponse actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals("La secuencia de ADN está vacía", actualResponseBody.getMessage());
    }

    @Test
    void test400ResponseInvalidDNASequence() {
        String[] dna = {"XTGCGA", "CAGTTC", "TTATGT", "AGAAGG", "TCTCTA", "TCACTG"};
        MutantRequest request = new MutantRequest(dna);

        ResponseEntity<MutantResponse> actualResponse = controller.mutant(request);
        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, actualResponse.getStatusCode());

        MutantResponse actualResponseBody = actualResponse.getBody();
        Assertions.assertNotNull(actualResponseBody);
        Assertions.assertEquals("Secuencia de ADN [XTGCGA] no válida. " +
                "Sólo puede contener los nucleobases A, T, C o G", actualResponseBody.getMessage());
    }
}