package co.com.mercadolibre.mutants.utils;

import co.com.mercadolibre.mutants.dto.MutantRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
class ValidationsTest {

    @Test
    void validations() {
        Assertions.assertThrows(MutantException.class, () -> Validations.validate(null));

        Assertions.assertThrows(MutantException.class, () -> {
            MutantRequest request = new MutantRequest();
            Validations.validate(request);
        });

        Assertions.assertThrows(MutantException.class, () -> {
            MutantRequest request = new MutantRequest(null);
            Validations.validate(request);
        });

        Assertions.assertThrows(MutantException.class, () -> {
            String[] dna = {};
            MutantRequest request = new MutantRequest(dna);
            Validations.validate(request);
        });

        Assertions.assertThrows(MutantException.class, () -> {
            String[] dna = {"ATGCGA", "CAGTGC", "TTJTGT", "AGAAGG", "CCCCTA", "TCAXTG"};
            MutantRequest request = new MutantRequest(dna);
            Validations.validate(request);
        });
    }
}