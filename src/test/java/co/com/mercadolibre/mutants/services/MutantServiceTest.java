package co.com.mercadolibre.mutants.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class MutantServiceTest {

    @Autowired
    private MutantService service;

    @Test
    void testIsMutant() {
        String[] dna = {
                "ATGCGA",
                "CAGTGC",
                "TTATGT",
                "AGAAGG",
                "CCCCTA",
                "TCACTG"
        };
        boolean result1 = service.isMutant(dna);
        assertTrue(result1);

        String[] dna2 = {
                "TTGCGA",
                "CAGTGC",
                "TTATGT",
                "AGAAGG",
                "CCCCTA",
                "TCACTG"
        };
        boolean result2 = service.isMutant(dna2);
        assertTrue(result2);

        String[] dna3 = {
                "TTGCGA",
                "CGGTGC",
                "TTATGT",
                "AGAAGG",
                "CCCCAA",
                "TCACTA"
        };
        boolean result3 = service.isMutant(dna3);
        assertTrue(result3);

        String[] dna4 = {
                "TTGCGA",
                "TGGTGA",
                "TTATGA",
                "TGAAGA",
                "CCCCAC",
                "TGGGGA"
        };
        boolean result4 = service.isMutant(dna4);
        assertTrue(result4);

        String[] dna5 = {
                "TTGCGA",
                "CCGTGC",
                "TTAAGT",
                "AGAAGT",
                "CACCTT",
                "ACACTT"
        };
        boolean result5 = service.isMutant(dna5);
        assertTrue(result5);
    }

    @Test
    void testIsNotMutant() {
        String[] dna = {
                "ATGCGA",
                "CAGTTC",
                "TTATGT",
                "AGAAGG",
                "TCTCTA",
                "TCACTG"
        };
        boolean result1 = service.isMutant(dna);
        assertFalse(result1);

        String[] dna2 = {
                "ATGCAA",
                "CAGTGC",
                "TTATGT",
                "AGAAGG",
                "TCCCTA",
                "TCACTG"
        };
        boolean result2 = service.isMutant(dna2);
        assertFalse(result2);


        String[] dna3 = {
                "ATGCAA",
                "CAGTGC",
                "TTATGT",
                "AGATGG",
                "TCCCTA",
                "TCACTG"
        };
        boolean result3 = service.isMutant(dna3);
        assertFalse(result3);
    }
}