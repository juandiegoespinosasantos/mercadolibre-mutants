package co.com.mercadolibre.mutants.services;

import co.com.mercadolibre.mutants.dto.StatsDTO;
import co.com.mercadolibre.mutants.enums.DNARecordType;
import co.com.mercadolibre.mutants.model.dao.DNARecordDao;
import co.com.mercadolibre.mutants.model.entities.DNARecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class DNARecordServiceTest {

    @Mock
    private DNARecordDao mockDao;

    private DNARecordService service;

    @BeforeEach
    void setUp() {
        service = new DNARecordService(mockDao);
    }

    @Test
    void testProcessSave() {
        DNARecord entity = getDNARecord();
        String dna = entity.getDna();

        Mockito.when(mockDao.findByDna(dna)).thenReturn(null);
        Mockito.when(mockDao.save(entity)).thenReturn(entity);

        String[] mutantDNA = {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};
        service.processSave(mutantDNA, true);

        Assertions.assertTrue(true);
    }

    @Test
    void testFindByDNA() {
        DNARecord expected = getDNARecord();
        String dna = "ATGCGA, CAGTGC, TTATGT, AGAAGG, CCCCTA, TCACTG";

        Mockito.when(mockDao.findByDna(dna)).thenReturn(expected);

        Optional<DNARecord> actualOpt = service.findByDNA(dna);
        Assertions.assertTrue(actualOpt.isPresent());

        DNARecord actual = actualOpt.get();
        Assertions.assertEquals(expected.getId(), actual.getId());
        Assertions.assertEquals(expected.getDna(), actual.getDna());
    }

    @Test
    void testFindAll() {
        DNARecord dnaRecord = getDNARecord();
        List<DNARecord> list = new ArrayList<>();
        list.add(dnaRecord);

        Mockito.when(mockDao.findAll()).thenReturn(list);

        List<DNARecord> actual = service.findAll();
        Assertions.assertEquals(1, actual.size());
    }

    @Test
    void testProcessStats() {
        List<DNARecord> list = new ArrayList<>();

        String dna1 = "ATGCGA, CAGTGC, TTATGT, AGAAGG, CCCCTA, TCACTG";
        DNARecord record1 = new DNARecord(dna1, DNARecordType.HOMO_SUPERIOR.name(), new Date());
        list.add(record1);

        String dna2 = "ATGCGA, CAGTGC, TTATGT, AGAAGG, AAAATA, TCACTG";
        DNARecord record2 = new DNARecord(dna2, DNARecordType.HOMO_SUPERIOR.name(), new Date());
        list.add(record2);

        String dna3 = "GTGCAA, CAGTGC, TTATGT, AGAAGG, ATTATA, TCACTG";
        DNARecord record3 = new DNARecord(dna3, DNARecordType.HOMO_SAPIENS.name(), new Date());
        list.add(record3);

        Mockito.when(mockDao.findAll()).thenReturn(list);

        StatsDTO actual = service.processStats();
        Assertions.assertEquals(2, actual.getCountMutantDna());
        Assertions.assertEquals(1, actual.getCountHumanDna());
        Assertions.assertEquals(2.0, actual.getRatio());
    }

    private DNARecord getDNARecord() {
        DNARecord dnaRecord = new DNARecord();
        dnaRecord.setId(1);
        dnaRecord.setType(DNARecordType.HOMO_SUPERIOR.name());
        dnaRecord.setCreationDatetime(new java.util.Date());
        dnaRecord.setDna("ATGCGA, CAGTGC, TTATGT, AGAAGG, CCCCTA, TCACTG");

        return dnaRecord;
    }
}