package co.com.mercadolibre.mutants.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MutantRequest implements Serializable {

    private static final long serialVersionUID = -3509712795104749760L;

    private String[] dna;
}
