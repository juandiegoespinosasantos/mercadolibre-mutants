package co.com.mercadolibre.mutants.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatsDTO implements Serializable {

    private static final long serialVersionUID = 161424945136174023L;

    private int countMutantDna;
    private int countHumanDna;
    private double ratio;
}
