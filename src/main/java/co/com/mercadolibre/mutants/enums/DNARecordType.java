package co.com.mercadolibre.mutants.enums;

/**
 *
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 20, 2022
 * @since 11
 */
public enum DNARecordType {

    HOMO_SUPERIOR, // Mutante
    HOMO_SAPIENS // Humano
}
