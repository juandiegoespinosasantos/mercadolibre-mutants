package co.com.mercadolibre.mutants.controllers;

import co.com.mercadolibre.mutants.dto.MutantRequest;
import co.com.mercadolibre.mutants.dto.MutantResponse;
import co.com.mercadolibre.mutants.services.DNARecordService;
import co.com.mercadolibre.mutants.services.MutantService;
import co.com.mercadolibre.mutants.utils.MutantException;
import co.com.mercadolibre.mutants.utils.Validations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller para validar información mutante
 *
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 20, 2022
 * @since 11
 */
@RestController
@RequestMapping("/mutant")
@CrossOrigin
public class MutantController {

    private final MutantService mutantService;
    private final DNARecordService dnaRecordService;

    @Autowired
    public MutantController(MutantService mutantService, DNARecordService dnaRecordService) {
        this.mutantService = mutantService;
        this.dnaRecordService = dnaRecordService;
    }

    /**
     * Valida si un ADN indicado es mutante o humano
     *
     * @param requestBody Objeto de la solicitud, con secuencia de ADN a analizar
     * @return Respuesta de la solicitud, indicando el estado del ADN
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MutantResponse> mutant(@RequestBody MutantRequest requestBody) {
        try {
            Validations.validate(requestBody);

            String[] dna = requestBody.getDna();
            boolean isMutant = mutantService.isMutant(dna);
            dnaRecordService.processSave(dna, isMutant);

            String message;
            HttpStatus httpStatus;

            if (isMutant) {
                message = "Magneto te da la bienvenida a la Hermandad de Mutantes";
                httpStatus = HttpStatus.OK;
            } else {
                message = "Lo siento, no puedes unirte a la causa mutante";
                httpStatus = HttpStatus.FORBIDDEN;
            }

            MutantResponse response = new MutantResponse(message);

            return new ResponseEntity<>(response, httpStatus);
        } catch (MutantException ex) {
            MutantResponse response = new MutantResponse(ex.getMessage());

            return new ResponseEntity<>(response, ex.getHttpStatus());
        }
    }
}
