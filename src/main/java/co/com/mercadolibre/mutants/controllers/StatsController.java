package co.com.mercadolibre.mutants.controllers;

import co.com.mercadolibre.mutants.dto.StatsDTO;
import co.com.mercadolibre.mutants.services.DNARecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller para consultar estadísticas mutantes
 *
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@RestController
@RequestMapping("/stats")
@CrossOrigin
public class StatsController {

    private final DNARecordService service;

    @Autowired
    public StatsController(DNARecordService service) {
        this.service = service;
    }

    /**
     * Obtiene las estadísticas de cantidad de ADN mutante y humano analizado
     *
     * @return Respuesta de la solicitud, con cantidad de ADN analizado por cada especie y radio mutante-humano
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatsDTO> stats() {
        StatsDTO stats = service.processStats();

        return ResponseEntity.ok(stats);
    }
}
