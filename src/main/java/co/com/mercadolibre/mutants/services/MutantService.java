package co.com.mercadolibre.mutants.services;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 20, 2022
 * @since 11
 */
@Service
public class MutantService {

    private static final int MIN_SEQUENCE_COUNT = 4;

    /**
     * Indica si la secuencia de ADN ingresada corresponde a un humano o
     * mutante, si una letra se encuentra en secuencia de forma horizontal,
     * vertical o diagonal
     *
     * @param dna Array de Strings que representan cada fila de una tabla de
     *            (NxN) con la secuencia del ADN. Las letras de los
     *            Strings solo pueden ser: (A, T, C, G).
     *            <p>
     *            Un humano es mutante, si encuentras ​ más de una secuencia de cuatro letras
     *            iguales​ , de forma oblicua, horizontal o vertical.
     * @return Valor booleano que indica si el ADN en mutante
     */
    public boolean isMutant(final String[] dna) {
        int countByRows = searchByRows(dna);
        int countByColumns = searchByColumns(dna);
        int countDiagonals = searchDiagonal(dna);

        // La búsqueda diagonal se hace en sentido hacia la derecha y abajo
        // Se invierte la lista para buscar si hay secuencias en sentido izquierda y arriba
        Collections.reverse(Arrays.asList(dna));
        int countReverseDiagonals = searchDiagonal(dna);

        return (countByRows + countByColumns + countDiagonals + countReverseDiagonals) > 1;
    }

    private int searchByRows(final String[] dna) {
        int length = dna.length;
        int found = 0;

        for (int row = 0; row < length; row++) {
            String currentRow = dna[row];

            for (int column = 0; column < length; column++) {
                char nucleobase = currentRow.charAt(column);
                int count = 1;

                for (int nextColumn = (column + 1); nextColumn < length; nextColumn++) {
                    char nextNucleobase = currentRow.charAt(nextColumn);
                    count = (nextNucleobase == nucleobase) ? (count + 1) : 0;
                    if (count == MIN_SEQUENCE_COUNT) found++;
                }
            }
        }

        return found;
    }

    private int searchByColumns(final String[] dna) {
        int matrixSize = dna.length;
        int found = 0;

        for (int row = 0; row < matrixSize; row++) {
            String currentRow = dna[row];

            for (int column = 0; column < matrixSize; column++) {
                char nucleobase = currentRow.charAt(column);
                int count = 1;

                for (int nextRow = (row + 1); nextRow < matrixSize; nextRow++) {
                    char nextNucleobase = dna[nextRow].charAt(column);
                    count = (nextNucleobase == nucleobase) ? (count + 1) : 0;
                    if (count == MIN_SEQUENCE_COUNT) found++;
                }
            }
        }

        return found;
    }

    private int searchDiagonal(final String[] dna) {
        int length = dna.length;
        int found = 0;

        for (int row = 0; row < length; row++) {
            String currentRow = dna[row];

            for (int column = 0; column < length; column++) {
                char nucleobase = currentRow.charAt(column);
                int count = 1;

                for (int next = (column + 1); next < (length - row - column); next++) {
                    char nextNucleobase = dna[row + next].charAt(column + next);
                    count = (nextNucleobase == nucleobase) ? count + 1 : 0;
                    if (count == MIN_SEQUENCE_COUNT) found++;
                }
            }
        }

        return found;
    }
}
