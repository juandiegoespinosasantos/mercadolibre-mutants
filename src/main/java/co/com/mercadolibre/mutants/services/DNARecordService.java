package co.com.mercadolibre.mutants.services;

import co.com.mercadolibre.mutants.dto.StatsDTO;
import co.com.mercadolibre.mutants.enums.DNARecordType;
import co.com.mercadolibre.mutants.model.dao.DNARecordDao;
import co.com.mercadolibre.mutants.model.entities.DNARecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Servicio para entidad DNARecord
 *
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Service
public class DNARecordService {

    private final DNARecordDao dao;

    @Autowired
    public DNARecordService(DNARecordDao dao) {
        this.dao = dao;
    }

    /**
     * Procesa el guardado de un registro de DNA analizado
     *
     * @param dnaSequence Array de Strings que representa la secuencia de ADN
     * @param isMutant    Booleano que indica si el ADN es mutante
     */
    public void processSave(final String[] dnaSequence, final boolean isMutant) {
        String dna = getDna(dnaSequence);
        DNARecordType type = getType(isMutant);
        java.util.Date now = new java.util.Date();

        // Como es un registro por ADN, se actualiza en caso de que ya existiera
        DNARecord entity = new DNARecord(dna, type.name(), now);
        findByDNA(dna).ifPresent(dnaRecord -> entity.setId(dnaRecord.getId()));

        dao.save(entity);
    }

    /**
     * Consulta un registro único por DNA
     *
     * @param dna Cadena de ADN de búsqueda
     * @return Opcional de la entidad encontrada
     */
    public Optional<DNARecord> findByDNA(String dna) {
        DNARecord dnaRecord = dao.findByDna(dna);

        return Optional.ofNullable(dnaRecord);
    }

    /**
     * Consulta todos los registros
     *
     * @return Lista de todos los registros almacenados
     */
    public List<DNARecord> findAll() {
        return (ArrayList<DNARecord>) dao.findAll();
    }

    /**
     * Genera el DTO de las estadísticas de cantidad de ADN mutante y humano analizado
     *
     * @return DTO con cantidad de ADN analizado por cada especie y radio mutante-humano
     */
    public StatsDTO processStats() {
        double countMutantDNA = 0;
        double countHumanDNA = 0;
        List<DNARecord> all = findAll();

        for (DNARecord dna : all) {
            if (dna.getType().equals(DNARecordType.HOMO_SUPERIOR.name())) {
                countMutantDNA++;
            } else {
                countHumanDNA++;
            }
        }

        double divisor = (countHumanDNA > 0.0) ? countHumanDNA : 1.0;
        double ratio = countMutantDNA / divisor;

        return new StatsDTO((int) countMutantDNA, (int) countHumanDNA, ratio);
    }

    private String getDna(final String[] dna) {
        return Arrays.asList(dna)
                .toString()
                .replace("[", "")
                .replace("]", "");
    }

    private DNARecordType getType(final boolean isMutant) {
        return isMutant ? DNARecordType.HOMO_SUPERIOR : DNARecordType.HOMO_SAPIENS;
    }
}
