package co.com.mercadolibre.mutants.utils;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Data
public class MutantException extends Exception {

    private static final long serialVersionUID = -8314034347306936160L;

    private final HttpStatus httpStatus;

    public MutantException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}