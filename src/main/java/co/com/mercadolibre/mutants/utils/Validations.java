package co.com.mercadolibre.mutants.utils;

import co.com.mercadolibre.mutants.dto.MutantRequest;
import org.springframework.http.HttpStatus;

/**
 * Validaciones
 *
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
public final class Validations {

    private static final String REGEX_DNA = "^[CAGTcagt]+$";

    private Validations() {

    }

    /**
     * Valida la solicitud de análisis de ADN
     *
     * @param requestBody Objeto de la solicitud, con secuencia de ADN a analizar
     * @throws MutantException Si se la solicitud ingresada presenta inconsistencias
     */
    public static void validate(final MutantRequest requestBody) throws MutantException {
        if (requestBody == null) {
            throw new MutantException(HttpStatus.BAD_REQUEST, "No indicaste el ADN a analizar");
        }

        String[] dna = requestBody.getDna();

        if ((dna == null) || (dna.length == 0)) {
            throw new MutantException(HttpStatus.BAD_REQUEST, "La secuencia de ADN está vacía");
        }

        for (String sequence : dna) {
            if (!sequence.matches(REGEX_DNA)) {
                throw new MutantException(HttpStatus.BAD_REQUEST, "Secuencia de ADN ["
                        + sequence + "] no válida. Sólo puede contener los nucleobases A, T, C o G");
            }
        }
    }
}