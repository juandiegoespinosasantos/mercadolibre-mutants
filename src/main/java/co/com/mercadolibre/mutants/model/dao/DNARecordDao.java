package co.com.mercadolibre.mutants.model.dao;

import co.com.mercadolibre.mutants.model.entities.DNARecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Repository
public interface DNARecordDao extends CrudRepository<DNARecord, Integer> {

    DNARecord findByDna(String dna);
}
