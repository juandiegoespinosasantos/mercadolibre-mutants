package co.com.mercadolibre.mutants.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author juandiegoespinosasantos@gmail.com
 * @version Mar 21, 2022
 * @since 11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dna_record")
public class DNARecord implements Serializable {

    private static final long serialVersionUID = -6381745690595318057L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "dna")
    private String dna;

    @Column(name = "type")
    private String type;

    @Column(name = "creation_datetime")
    private java.util.Date creationDatetime;

    public DNARecord(String dna, String type, java.util.Date creationDatetime) {
        this.dna = dna;
        this.type = type;
        this.creationDatetime = creationDatetime;
    }
}
